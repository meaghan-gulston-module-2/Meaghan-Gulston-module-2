class App{
  var nameofApp;
  var category;
  var developers;
  var yearWon;
  
  showAppDetails(){
    print('The name of the app is $nameofApp.');
    print('The category that the app won in is $category.');
    print('The developers for $nameofApp are $developers.');
    print('The year that the app won in was in $yearWon.');
  }
  
 nameUpperCase(){
   print('${nameofApp.toUpperCase()}');
 }
}

void main(){
  var app= new App();
  app.nameofApp="Naked Insurance";
  app.category="Overall and The Best Financial Solution Award";
  app.developers="Sumarie Greybe, Ernest North, and Alex Thomson";
  app.yearWon=2019;
  print('Here are the details for the winning app:');
  app.showAppDetails();
  app.nameUpperCase();
  

}

